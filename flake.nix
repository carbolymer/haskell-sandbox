{
  description = "haskell sandbox";

  inputs = {
    systems.url = "github:nix-systems/x86_64-linux";
    haskellNix.url = "github:input-output-hk/haskell.nix";
    nixpkgs.follows = "haskellNix/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
    utils.inputs.systems.follows = "systems";
    cachix.url = "github:cachix/cachix/latest";
    cachix.inputs.nixpkgs.follows = "haskellNix";
  };

  outputs = { nixpkgs, cachix, utils, haskellNix, ... }:
    utils.lib.eachDefaultSystem (system:
      let
        overlays = [
          haskellNix.overlay
          (final: prev: {
            haskellSandbox = final.haskell-nix.hix.project {
              src = ./.;
              compiler-nix-name = "ghc910";
              shell.tools = {
                cabal = "latest";
                # broken with GHC 9.10 for now
                # hlint = "latest";
                haskell-language-server = "latest";
              };
              shell.buildInputs = with pkgs; [
                nixfmt-rfc-style
                ( # fix missing haskell-language-server-wrapper binary
                  writeShellScriptBin "haskell-language-server-wrapper"
                  "haskell-language-server $@")
              ];
              # for some reason without this, ghci config breaks
              shell.shellHook = ''
                export LANG="en_GB.UTF-8"
              '' + pkgs.lib.optionalString (pkgs.glibcLocales != null
                && pkgs.stdenv.hostPlatform.libc == "glibc") ''
                  export LOCALE_ARCHIVE="${pkgs.glibcLocales}/lib/locale/locale-archive"
                '';
              shell.exactDeps = true;
            };
          })
        ];
        pkgs = import nixpkgs {
          inherit system overlays;
          inherit (haskellNix) config;
        };
        cachixBin = (import cachix).packages.${system}.cachix + "/bin/cachix";
        flake = pkgs.haskellSandbox.flake { };
        defaultPackage = flake.packages."haskell-sandbox:exe:sandbox";
      in flake // {
        legacyPackages = pkgs;

        packages.default = defaultPackage;

        apps.ci-buildPushCache = utils.lib.mkApp {
          drv = pkgs.writeShellScriptBin "buildPushCache" ''
            #!/usr/bin/env bash
            which cachix
            nix path-info --recursive "${defaultPackage}" | ${cachixBin} push display-input-switch
          '';
        };

        formatter = pkgs.nixfmt-classic;
      });

  nixConfig = {
    substituters = [
      "https://cache.nixos.org"
      "https://cache.iog.io"
      "https://display-input-switch.cachix.org"
    ];
    trusted-public-keys = [
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
      "display-input-switch.cachix.org-1:oEzHptD2Bpfts7LNgnNnRGaQRpjBCFVS1oX7Olir8RM="
    ];
    # For some reason this does not work, flake show needs to be run:
    # nix --allow-import-from-derivation flake show
    allow-import-from-derivation = true;
  };
}
