# Haskell Sandbox

Handy repl with useful utilities. Just use `cabal repl`.

## Using nix

Run `nix develop -c cabal repl`
